import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { AdminComponent } from './dashboard/admin/admin.component';
import { ManagementComponent } from './dashboard/management/management.component';
import { AttendanceComponent } from './dashboard/attendance/attendance.component';

import { AdminNavbarComponent } from './dashboard/admin/admin-navbar/admin-navbar.component';
import { DirectorComponent } from './dashboard/admin/director/director.component';
import { UbsComponent } from './dashboard/admin/ubs/ubs.component';
import { NewDirectorComponent } from './dashboard/admin/director/new-director/new-director.component';
import { NewUbsComponent } from './dashboard/admin/ubs/new-ubs/new-ubs.component';
import { ManageUbsComponent } from './dashboard/admin/ubs/manage-ubs/manage-ubs.component';
import { ManageDirectorComponent } from './dashboard/admin/director/manage-director/manage-director.component';

import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { NgxBrModule } from '@nbfontana/ngx-br';
import { PharmaceuticalComponent } from './dashboard/pharmaceutical/pharmaceutical.component';
import { PharmaNavbarComponent } from './dashboard/pharmaceutical/pharma-navbar/pharma-navbar/pharma-navbar.component';


const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'admin/ubs', component: UbsComponent },
  { path: 'admin/ubs/new', component: NewUbsComponent },
  { path: 'admin/ubs/manage', component: ManageUbsComponent },
  { path: 'admin/director', component: DirectorComponent },
  { path: 'admin/director/new', component: NewDirectorComponent },
  { path: 'admin/director/manage', component: ManageDirectorComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    DashboardComponent,
    AttendanceComponent,
    ManagementComponent,
    AdminComponent,
    UbsComponent,
    DirectorComponent,
    AdminNavbarComponent,
    NewUbsComponent,
    ManageUbsComponent,
    NewDirectorComponent,
    ManageDirectorComponent,
    PharmaceuticalComponent,
    PharmaNavbarComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
    ),
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxBrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
