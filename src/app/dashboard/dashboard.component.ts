import { Component } from '@angular/core';
import { funcionario } from '../classes';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  funcionario: funcionario;

  constructor(private router: Router) {
    if (sessionStorage.getItem('funcionario') === null) {
      this.router.navigate(['']);
    } else if (this.funcionario === undefined){
      this.funcionario = JSON.parse(sessionStorage.getItem('funcionario'));
    }
  }
}
