import { Component } from '@angular/core';
import { funcionario } from 'src/app/classes';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent {

  funcionario: funcionario;
  // atividadeFiltrada: Array<string>;

  constructor() {
    // this.atividadeFiltrada = [];
    this.funcionario = JSON.parse(sessionStorage.getItem('funcionario'));
    console.log(this.funcionario);

    // this.atividadeFiltrada = this.funcionario.log.slice(0, 8);
    // console.log(this.atividadeFiltrada);
  }
}
