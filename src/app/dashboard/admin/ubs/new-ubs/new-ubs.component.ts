import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-new-ubs',
  templateUrl: './new-ubs.component.html',
  styleUrls: ['./new-ubs.component.scss']
})
export class NewUbsComponent {

  newUbsForm: any;
  directorList = [
    '',
    'Matheus Leandro',
    'Erick Bontempo',
    'Jorge Yaqub',
    'Monica Leandro'
  ];


  constructor(private fb: FormBuilder) {
    this.createForm();
  }

  createForm() {
    this.newUbsForm = this.fb.group({
      id: null,
      nome: 'UBS ',
      diretor: ''
    });
  }
}
