import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-new-director',
  templateUrl: './new-director.component.html',
  styleUrls: ['./new-director.component.scss']
})
export class NewDirectorComponent {

  directorForm: any;
  ubsList = [
    '',
    'UBS Tranquilidade',
    'UBS Cavadas',
    'UBS Vila Galvão',
    'UBS São Rafael',
    'UBS Jardim Palmira',
    'UBS Continental',
    'UBS Cumbica',
    'UBS Pimentas',
    'UBS São João de Boa Vista'
  ];

  constructor(private fb: FormBuilder) {
    this.createForm();
  }

  createForm() {
    this.directorForm = this.fb.group({
    id: null,
    nome: '',
    cargo: 'Diretor',
    cpf: '',
    password: 'ubs123',
    unidade: '',
    log: []
    });
  }

}
