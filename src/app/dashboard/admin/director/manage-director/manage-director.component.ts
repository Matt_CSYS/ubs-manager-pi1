import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-manage-director',
  templateUrl: './manage-director.component.html',
  styleUrls: ['./manage-director.component.scss']
})
export class ManageDirectorComponent implements OnInit {

  cpf = '400.727.958-67';

  directorList = [
    'Matheus Leandro',
    'Erick Bontempo',
    'Jorge Yaqub Boutchakdjian Milagre',
    'Monica Leandro',
    'Manuela Leandro Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann',
    'Matheus Leandro II Winkelmann'
  ];

  constructor() { }

  ngOnInit() {
  }

}
