import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})

export class LoginComponent {
    loginForm: any;
    formError: boolean;

    constructor(private fb: FormBuilder, private loginService: LoginService, private router: Router) {
        this.createForm();
        this.formError = false;
    }

    createForm() {
        this.loginForm = this.fb.group({
            codFunc: '',
            senha: ''
        });
    }

    login() {
        if (this.loginForm.value.codFunc.length < 6 || this.loginForm.value.senha.length < 6) {
            this.formError = true;
            return;
        }

        this.formError = false;
        const temp = this.loginService.identify(this.loginForm.value);
        sessionStorage.setItem('funcionario', JSON.stringify(temp));
        this.router.navigate(['dashboard']);
        // this.loginService.identify(this.loginForm.value).subscribe(
        //   (response) => {
        //     if (response) {
        //       console.log(response);
        //       sessionStorage.setItem('funcionario', JSON.stringify(response));
        //       this.router.navigate(['dashboard']);
        //     }
        //   },
        //   () => {
        //     this.formError = true;
        //   });
    }
}
