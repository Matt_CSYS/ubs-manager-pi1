export class funcionario {
    id: number;
    nome: string;
    cpf: number;
    password: string;
    cargo: string;
    unidade: string;
    log: Array<string>;
}
