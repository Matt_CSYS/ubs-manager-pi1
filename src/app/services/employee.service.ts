import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  list(perm) {
    const body = {
      permissao: perm
    }
    return this.http.post('localhost:8080/api/funcionario/pesquisarFuncionarioCargo', body)
  }

  insert(employeeData: any) {
    const body = {
      codFunc: employeeData.cod,
      nome: employeeData.nome,
      dataNasc: employeeData.nasc,
      codCargo: employeeData.cargo,
      cpf: employeeData.cpf,
      permissao: employeeData.permissao,
      telefone: employeeData.telefone,
      endereco: employeeData.endereco,
      senha: employeeData.senha,
      unidade: employeeData.unidade
    }
    return this.http.post('localhost:8080/api/funcionario/cadastrarFuncionario', body);
  }

  alter(employeeData: any, newData: any) {
    const body = {
      codFunc: employeeData.cod,
      nome: employeeData.nome,
      dataNasc: employeeData.nasc,
      codCargo: employeeData.cargo,
      cpf: employeeData.cpf,
      permissao: employeeData.permissao,
      telefone: employeeData.telefone,
      endereco: employeeData.endereco,
      senha: employeeData.senha,
      unidade: employeeData.unidade
    }
    return this.http.post('localhost:8080/api/funcionario/atualizarFuncionario', body);
  }

  delete(employeeData: any) {
    const body = {
      codFunc: employeeData.cod,
    }
    return this.http.post('localhost:8080/api/funcionario/deletarFuncionario', body);
  }
}
