import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UbsService {

  constructor(private http: HttpClient) { }

  insert(ubsData: any) {
    const body = {
      nome: ubsData.nome,
      endereco: ubsData.endereco
    }
    return this.http.post('localhost:8080/api/cadastrarUbs', body);
  }

  alter(ubsData: any, newData: any) {
    const body = {
      cod_unidade: ubsData.id,
      nome: newData.nome,
      endereco: newData.endereco
    }
    return this.http.post('localhost:8080/api/atualizarUbs', body);
  }

  delete(ubsData: any) {
    const body = {
      cod_unidade: ubsData.id,
    }
    return this.http.post('localhost:8080/api/deletarUbs', body);
  }
}
